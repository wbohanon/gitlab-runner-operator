#!/bin/bash

set -eo pipefail

source scripts/_common.sh

echo "Tagging and pushing UBI images"

# Push the UBI images only if the images aren't already in the registry
# they are separate from the operator images with separate versions
# we could have two different versions of the operator that use the same runner and helper
# images. In that case we don't want our pipelines to fail because the image already exists.
if ! docker pull "$GITLAB_RUNNER_IMAGE"; then
  RUNNER_UBI_IMAGE="$UPSTREAM_UBI_IMAGES_REPOSITORY/gitlab-runner-ocp:$RUNNER_REVISION"
  docker pull "$RUNNER_UBI_IMAGE"
  docker tag "$RUNNER_UBI_IMAGE" "$GITLAB_RUNNER_IMAGE"
  login_and_push runner "$CERTIFIED"
fi

if ! docker pull "$GITLAB_RUNNER_HELPER_IMAGE"; then
  RUNNER_HELPER_UBI_IMAGE="$UPSTREAM_UBI_IMAGES_REPOSITORY/gitlab-runner-helper-ocp:x86_64-$RUNNER_REVISION"
  docker pull "$RUNNER_HELPER_UBI_IMAGE"
  docker tag "$RUNNER_HELPER_UBI_IMAGE" "$GITLAB_RUNNER_HELPER_IMAGE"
  login_and_push helper "$CERTIFIED"
fi
