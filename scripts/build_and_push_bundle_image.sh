#!/bin/bash

set -eo pipefail

source scripts/_common.sh

if "$CERTIFIED"; then
  PROJECT="$GITLAB_RUNNER_OPERATOR_CONNECT_NAMESPACE"
else
  PROJECT="$GITLAB_RUNNER_OPERATOR_UPSTREAM_NAMESPACE"
fi

echo "Building bundle for operator image $PROJECT/$GITLAB_RUNNER_OPERATOR_BUNDLE_IMAGE"

make IMG="$PROJECT/$GITLAB_RUNNER_OPERATOR_IMAGE" bundle
source ./scripts/create_related_images_config.sh
source ./scripts/set_csv_versions.sh

make certification-bundle
make certification-bundle-build
make certification-bundle-tag PROJECT="$PROJECT"
login_and_push bundle "$CERTIFIED"