#!/bin/bash

set -eo pipefail

source scripts/_common.sh
source scripts/create_release_config.sh

if "$CERTIFIED"; then
  PROJECT="$GITLAB_RUNNER_OPERATOR_CONNECT_NAMESPACE"
else
  PROJECT="$GITLAB_RUNNER_OPERATOR_UPSTREAM_NAMESPACE"
fi

echo "Building operator image $PROJECT/$GITLAB_RUNNER_OPERATOR_IMAGE"

make docker-build
make docker-tag PROJECT="$PROJECT"
login_and_push operator "$CERTIFIED"