#!/bin/bash

set -eo pipefail

source scripts/_common.sh

echo "Creating CSV at bundle/patches/related_images.yaml"

if "$CERTIFIED"; then
  cat > bundle/patches/related_images.yaml << EOF
  apiVersion: operators.coreos.com/v1alpha1
  kind: ClusterServiceVersion
  metadata:
    name: gitlab-runner-operator:$OPERATOR_VERSION_CLEAN
  spec:
    relatedImages:
    - name: gitlab-runner
      image: registry.connect.redhat.com/gitlab/gitlab-runner$(image_digest "runner")
    - name: gitlab-runner-helper
      image: registry.connect.redhat.com/gitlab/gitlab-runner-helper$(image_digest "helper")
    - name: gitlab-runner-operator
      image: registry.connect.redhat.com/gitlab/gitlab-runner-operator$(image_digest "operator")
    - name: kube-rbac-proxy
      image: $KUBE_RBAC_PROXY_IMAGE
EOF
else
  cat > bundle/patches/related_images.yaml << EOF
  apiVersion: operators.coreos.com/v1alpha1
  kind: ClusterServiceVersion
  metadata:
    name: gitlab-runner-operator:$OPERATOR_VERSION_CLEAN
  spec:
    relatedImages:
    - name: gitlab-runner
      image: $UPSTREAM_UBI_IMAGES_REPOSITORY/gitlab-runner-ocp:$RUNNER_REVISION
    - name: gitlab-runner-helper
      image: $UPSTREAM_UBI_IMAGES_REPOSITORY/gitlab-runner-helper-ocp:x86_64-$RUNNER_REVISION
    - name: gitlab-runner-operator
      image: $UPSTREAM_OPERATOR_IMAGES_REPOSITORY/$GITLAB_RUNNER_OPERATOR_BUNDLE_IMAGE
    - name: kube-rbac-proxy
      image: $KUBE_RBAC_PROXY_IMAGE
EOF
fi

echo "bundle/patches/related_images.yaml:"
cat bundle/patches/related_images.yaml