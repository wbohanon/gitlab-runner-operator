package utils

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"sigs.k8s.io/yaml"
)

const releaseFile = "release.yaml"

// Image represents a
// single microservice image
type Image struct {
	Name      string `yaml:"name"`
	Upstream  string `yaml:"upstream"`
	Certified string `yaml:"certified"`
}

// Image function auto-selects image to deploy
func (img Image) Image() string {
	if IsOpenshift() &&
		img.Certified != "" {
		return img.Certified
	}

	return img.Upstream
}

// Release defines a GitLab release
type Release struct {
	Version string  `json:"version" yaml:"version"`
	Images  []Image `json:"images" yaml:"images"`
}

//GetRelease reads the release file and returns a release object
func GetRelease() *Release {
	dir, err := getReleaseDir()
	if err != nil {
		fmt.Printf("error getting releases directory; %v\n", err)
	}

	releaseFile, err := ioutil.ReadFile(filepath.Join(dir, releaseFile))
	if err != nil {
		return nil
	}

	release := &Release{}
	err = yaml.Unmarshal(releaseFile, release)
	if err != nil {
		return nil
	}

	return release
}

func getReleaseDir() (string, error) {
	_, err := os.Stat("/run/secrets/kubernetes.io/serviceaccount")
	if err == nil {
		return "/", nil
	}

	wd, err := os.Getwd()
	if err != nil {
		return "", err
	}

	if strings.HasSuffix(wd, "gitlab-runner-operator") {
		return filepath.Join(wd, "hack/assets"), nil
	}

	// check if path is project dir is in path but not last dir
	var basePath string
	if strings.Contains(wd, "gitlab-runner-operator") {
		dirs := strings.SplitAfter(wd, "gitlab-runner-operator")
		basePath = dirs[0]
	}

	return filepath.Join(basePath, "hack/assets"), nil
}
